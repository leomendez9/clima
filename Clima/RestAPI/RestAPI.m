//
//  RestAPI.m
//  Clima
//
//  Created by Leonardo Mendez on 9/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import "RestAPI.h"

@interface RestAPI() <NSURLConnectionDelegate,NSURLSessionDelegate>

@property (strong, nonatomic) NSMutableData *recivedData;
@property (strong, nonatomic) NSURLConnection *requestConnection;
@end

@implementation RestAPI

- (NSMutableData *)recivedData{
    if (!_recivedData){
        _recivedData = [[NSMutableData alloc] init];
    }
    return _recivedData;
}

- (NSURLConnection *)requestConnection{
    if (!_requestConnection){
        _requestConnection = [[NSURLConnection alloc] init];
    }
    return _requestConnection;
}

- (void)httpRequest:(NSMutableURLRequest *)request{
    self.requestConnection = [NSURLConnection connectionWithRequest:request delegate:self];
}

//metodos de request
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [self.recivedData appendData:data];
}

//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    //   _responseData = [[NSMutableData alloc] init];
//}

//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
//                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
//    
//    return nil;
//}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.delegate getRecivedData:self.recivedData sender:self];
    self.delegate = nil;
    self.requestConnection = nil;
    self.recivedData = nil;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@",error.description);
}
@end
