//
//  RestAPI.h
//  Clima
//
//  Created by Leonardo Mendez on 9/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RestAPI;
@protocol RestAPIDelegate
- (void)getRecivedData:(NSMutableData *)data sender:(RestAPI *)sender;
@end

@interface RestAPI : NSObject
- (void)httpRequest:(NSMutableURLRequest *)request;

@property (weak,nonatomic) id  <RestAPIDelegate> delegate;
@end

#define POST @"POST"
#define GET @"GET"