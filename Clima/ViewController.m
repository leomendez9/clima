//
//  ViewController.m
//  Clima
//
//  Created by Leonardo Mendez on 9/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import "ViewController.h"
#import "RestAPI.h"
#import "SearchViewController.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

static NSString * const cUrl = @"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=693b0c0df4e02e4957f4ee6cc03387e3&units=metric&lang=es";


@interface ViewController () <NSURLConnectionDelegate>{
    NSMutableData *_responseData;

}
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) RestAPI *restApi;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hourBackground];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.mapView.showsUserLocation = YES;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (RestAPI *)restApi{
    if (!_restApi) {
        _restApi = [[RestAPI alloc]init];
    }
    return _restApi;
}

- (void)hourBackground{
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    if ([newDateString isEqualToString:@"00"]) {
       newDateString = @"24";
    }
    
    int hour = [newDateString intValue];
    
    if (hour >= 19 || hour <= 5) {
        self.imageBackground.image = [UIImage imageNamed:@"night.jpg"];
        [self.cityTextField setTextColor:[UIColor whiteColor]];
        [self.latitudeTextField setTextColor:[UIColor whiteColor]];
        [self.lengthTextField setTextColor:[UIColor whiteColor]];
        [self.weatherTextField setTextColor:[UIColor whiteColor]];
        [self.degreesTextField setTextColor:[UIColor whiteColor]];
        self.cityLabel.textColor = [UIColor whiteColor];
        self.latitudeLabel.textColor = [UIColor whiteColor];
        self.lengthLabel.textColor = [UIColor whiteColor];
        self.weatherLabel.textColor = [UIColor whiteColor];
        self.degreesLabel.textColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else{
         self.imageBackground.image = [UIImage imageNamed:@"day.jpg"];
    }
    if (hour == 06) {
        self.imageBackground.image = [UIImage imageNamed:@"dawn.jpg"];
    }
}

- (void)httpGetRequest:(NSString *)lat value:(NSString *)lon{
    NSString *cURL = [NSString stringWithFormat:cUrl,lat,lon];
    
    [self.activityIndicator startAnimating];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:cURL]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (error)
                {
                    NSLog(@"Error,%@", [error localizedDescription]);
                }
                else
                {
                    NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    float num1 = [[[jsonData objectForKey:@"main"] objectForKey:@"temp"] floatValue];
                    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
                    [formatter setMaximumFractionDigits:0];
                    self.degreesTextField.text = [NSString stringWithFormat:@"%@ºC",[formatter stringFromNumber:[NSNumber numberWithFloat:num1]]];
                    self.cityTextField.text = [NSString stringWithFormat:@"%@",[jsonData objectForKey:@"name"]];
                    self.latitudeTextField.text = lat;
                    self.lengthTextField.text = lon;
                    self.weatherTextField.text =[NSString stringWithFormat:@"%@",[[[jsonData objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"]];
                    [self.activityIndicator stopAnimating];
                }
            }] resume];
}

#pragma locationManager Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation *crnLoc = [locations lastObject];
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = crnLoc.coordinate.latitude;
    location.longitude = crnLoc.coordinate.longitude;
    
    [self httpGetRequest:[NSString stringWithFormat:@"%f", [[locations lastObject] coordinate].latitude] value:[NSString stringWithFormat:@"%f", [[locations lastObject] coordinate].longitude]];
    
    region.span = span;
    region.center = location;
    [self.mapView setRegion:region animated:YES];
    [self.mapView regionThatFits:region];
//    [self.locationManager stopUpdatingLocation];

}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
}
- (IBAction)search:(id)sender {
    NSString * storyboardName = @"Search";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    SearchViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"searchCity"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
