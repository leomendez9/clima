//
//  ResultViewController.h
//  Clima
//
//  Created by Leonardo Mendez on 10/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController
@property (strong, nonatomic) NSDictionary *jsonDataRecive;
@property (strong, nonatomic) NSDictionary *jsonDataRecive2;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *degreesLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel2;
@property (weak, nonatomic) IBOutlet UILabel *degreesLabel2;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (weak, nonatomic) IBOutlet UILabel *resultTextLabel;

@end
