//
//  ResultViewController.m
//  Clima
//
//  Created by Leonardo Mendez on 10/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import "ResultViewController.h"

static NSString * const cResultText = @"Basado en la busquedad del clima de la ciudad %@ y la ciudad de %@, se le recomienda ir de vacaciones a la ciudad %@ ya que posee mejor clima.";
static NSString * const cResultText2 = @"Basado en la busquedad del clima de la ciudad %@ y la ciudad de %@, se le recomienda ir de vacaciones a cualquiera de las dos ciudad ya que posee un clima parecido.";
static NSString * const cCity = @"Ciudad: %@";
static NSString * const cLatitude = @"Latitud: %@";
static NSString * const cLongitude = @"Longitud: %@";
static NSString * const cWeather = @"Clima: %@";
static NSString * const cDegrees = @"Grados: %@ºC";
static NSString * const cIdealTemperature = @"25";

@interface ResultViewController ()
@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hourBackground];
    [self result];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)hourBackground{
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    if ([newDateString isEqualToString:@"00"]) {
        newDateString = @"24";
    }
    
    int hour = [newDateString intValue];
    
    if (hour >= 19 || hour <= 5) {
        self.imageBackground.image = [UIImage imageNamed:@"night.jpg"];
        self.cityNameLabel.textColor = [UIColor whiteColor];
        self.latitudeLabel.textColor = [UIColor whiteColor];
        self.longitudeLabel.textColor = [UIColor whiteColor];
        self.weatherLabel.textColor = [UIColor whiteColor];
        self.degreesLabel.textColor = [UIColor whiteColor];
        self.cityNameLabel2.textColor = [UIColor whiteColor];
        self.latitudeLabel2.textColor = [UIColor whiteColor];
        self.longitudeLabel2.textColor = [UIColor whiteColor];
        self.weatherLabel2.textColor = [UIColor whiteColor];
        self.degreesLabel2.textColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        [self.backButton setImage:[UIImage imageNamed:@"whiteBack.png"] forState:UIControlStateNormal];
        self.separator.backgroundColor = [UIColor whiteColor];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else{
        self.imageBackground.image = [UIImage imageNamed:@"day.jpg"];
    }
    if (hour == 06) {
        self.imageBackground.image = [UIImage imageNamed:@"dawn.jpg"];
    }
}

- (void)result{
    float num1 = [[[self.jsonDataRecive objectForKey:@"main"] objectForKey:@"temp"] floatValue];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    [formatter setMaximumFractionDigits:0];
    
    self.degreesLabel.text = [NSString stringWithFormat:cDegrees,[formatter stringFromNumber:[NSNumber numberWithFloat:num1]]];
    self.cityNameLabel.text = [NSString stringWithFormat:cCity,[self.jsonDataRecive objectForKey:@"name"]];
    self.latitudeLabel.text = [NSString stringWithFormat:cLatitude,[[self.jsonDataRecive objectForKey:@"coord"]objectForKey:@"lat"]];
    self.longitudeLabel.text = [NSString stringWithFormat:cLongitude,[[self.jsonDataRecive objectForKey:@"coord"]objectForKey:@"lon"]];
    self.weatherLabel.text =[NSString stringWithFormat:cWeather,[[[self.jsonDataRecive objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"]];
    
    float num2 = [[[self.jsonDataRecive2 objectForKey:@"main"] objectForKey:@"temp"] floatValue];
    NSNumberFormatter *formatter2 = [[NSNumberFormatter alloc] init];
    [formatter2 setRoundingMode:NSNumberFormatterRoundHalfUp];
    [formatter2 setMaximumFractionDigits:0];

    self.degreesLabel2.text = [NSString stringWithFormat:cDegrees,[formatter stringFromNumber:[NSNumber numberWithFloat:num2]]];
    self.cityNameLabel2.text = [NSString stringWithFormat:cCity,[self.jsonDataRecive2 objectForKey:@"name"]];
    self.latitudeLabel2.text = [NSString stringWithFormat:cLatitude,[[self.jsonDataRecive2 objectForKey:@"coord"]objectForKey:@"lat"]];
    self.longitudeLabel2.text = [NSString stringWithFormat:cLongitude,[[self.jsonDataRecive2 objectForKey:@"coord"]objectForKey:@"lon"]];
    self.weatherLabel2.text =[NSString stringWithFormat:cWeather,[[[self.jsonDataRecive2 objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"]];
    
    int city = 0;
    
    float cityDegrees1 = [self calculateDistanceDegrees:num1];
    float cityDegrees2 = [self calculateDistanceDegrees:num2];
    
    
    if (cityDegrees1 < cityDegrees2) {
        city = 1;
    }
    
    if (cityDegrees2 < cityDegrees1){
       city = 2;
    }
    
    if (cityDegrees1 == cityDegrees2) {
        city = 3;
    }
    
    switch (city) {
        case 1:
            self.resultTextLabel.text = [NSString stringWithFormat:cResultText,[self.jsonDataRecive objectForKey:@"name"],[self.jsonDataRecive2 objectForKey:@"name"], [self.jsonDataRecive objectForKey:@"name"]];
            break;
        case 2:
             self.resultTextLabel.text = [NSString stringWithFormat:cResultText,[self.jsonDataRecive objectForKey:@"name"],[self.jsonDataRecive2 objectForKey:@"name"], [self.jsonDataRecive2 objectForKey:@"name"]];
            break;

        case 3:
            self.resultTextLabel.text = [NSString stringWithFormat:cResultText2,[self.jsonDataRecive objectForKey:@"name"],[self.jsonDataRecive2 objectForKey:@"name"]];
            break;
        
        default:
            break;
    }
    
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)start:(id)sender {
     [self.navigationController popToRootViewControllerAnimated:YES];
}

- (float)calculateDistanceDegrees:(float)num{
    
    float result = 0;
    float  iT = [cIdealTemperature floatValue];
    
    if (num > iT) {
        result = num - iT;
    }else{
        if (num < 0) {
            result = iT + (num * -1);
        }else{
            result = iT - num;
        }
    }
    return result;
}

@end
