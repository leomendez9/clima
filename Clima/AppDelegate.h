//
//  AppDelegate.h
//  Clima
//
//  Created by Leonardo Mendez on 9/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

