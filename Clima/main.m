//
//  main.m
//  Clima
//
//  Created by Leonardo Mendez on 9/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
