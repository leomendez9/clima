//
//  SearchViewController.m
//  Clima
//
//  Created by Leonardo Mendez on 10/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import "SearchViewController.h"
#import "ViewController.h"
#import "ResultViewController.h"
#import "RestAPI.h"

static NSString * const cUrl = @"http://api.openweathermap.org/data/2.5/weather?q=%@&appid=693b0c0df4e02e4957f4ee6cc03387e3&units=metric&lang=es";
static NSString * const cUrl2 = @"http://api.openweathermap.org/data/2.5/weather?q=%@&appid=693b0c0df4e02e4957f4ee6cc03387e3&units=metric&lang=es";
static NSString * const cAlertTitle = @"Error";
static NSString * const cAlertMessage= @"La casilla de la ciudad 1 esta vacía o es incorrecta";
static NSString * const cAlertMessage2 = @"La casilla de la ciudad 2 esta vacía o es incorrecta";

@interface SearchViewController ()
@property (strong, nonatomic) NSDictionary *jsonData;
@property (strong, nonatomic) NSDictionary *jsonData2;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hourBackground];
    
    self.searchButton.layer.cornerRadius = 5;
    self.searchButton.layer.borderWidth = 1;
    self.searchButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    [self.activityIndicator stopAnimating];
    self.searchButton.enabled = NO;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)hourBackground{
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    if ([newDateString isEqualToString:@"00"]) {
        newDateString = @"24";
    }
    
    int hour = [newDateString intValue];
    
    if (hour >= 19 || hour <= 5) {
        self.imageBackground.image = [UIImage imageNamed:@"night.jpg"];
        self.city1Label.textColor = [UIColor whiteColor];
        self.city2Label.textColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.searchButton.layer.borderColor = [[UIColor whiteColor]CGColor];
        [self.searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.backButton setImage:[UIImage imageNamed:@"whiteBack.png"] forState:UIControlStateNormal];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
    }else{
        self.imageBackground.image = [UIImage imageNamed:@"day.jpg"];
    }
    if (hour == 6) {
        self.imageBackground.image = [UIImage imageNamed:@"dawn.jpg"];
    }
}


- (IBAction)toCheckTextTield:(id)sender{
    if (![self.city1TextField.text isEqualToString:@""] && ![self.city2TextField.text isEqualToString:@""]) {
        self.searchButton.enabled = YES;
    }else {
        self.searchButton.enabled = NO;

    }
    
}

- (void)httpGetRequestWhitCity1:(NSString *)city1 city2:(NSString *)city2{
   
    NSString *cURL = [NSString stringWithFormat:cUrl,city1];
    
    [self.activityIndicator startAnimating];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:cURL]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (error){
                    NSLog(@"Error,%@", [error localizedDescription]);
                    
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:cAlertTitle
                                                 message:cAlertMessage
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* OkButton = [UIAlertAction
                                               actionWithTitle:@"ok"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                               }];
                    [alert addAction:OkButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    [self.activityIndicator stopAnimating];
                }else{
                    NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    self.jsonData = jsonData;
                    
                    NSString *cURL2 = [NSString stringWithFormat:cUrl2,city2];
                    
                    NSURLSession *session2 = [NSURLSession sharedSession];
                    [[session2 dataTaskWithURL:[NSURL URLWithString:cURL2]
                             completionHandler:^(NSData *data2,
                                                 NSURLResponse *response2,
                                                 NSError *error2) {
                                 if (error2){
                                     NSLog(@"Error,%@", [error2 localizedDescription]);
                                     
                                     UIAlertController * alert=   [UIAlertController
                                                                   alertControllerWithTitle:cAlertTitle
                                                                   message:cAlertMessage2
                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* OkButton = [UIAlertAction
                                                                actionWithTitle:@"ok"
                                                                style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action){
                                                                }];
                                     [alert addAction:OkButton];
                                     [self presentViewController:alert animated:YES completion:nil];
                                     [self.activityIndicator stopAnimating];
                                 }
                                 else{
                                     NSDictionary* jsonData2 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                                     self.jsonData2 = jsonData2;
                                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                         [self.activityIndicator stopAnimating];
                                         
                                         NSString * storyboardName = @"Result";
                                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                                         ResultViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"resultCity"];
                                         vc.jsonDataRecive  = self.jsonData;
                                         vc.jsonDataRecive2 = self.jsonData2;
                                         [self.navigationController pushViewController:vc animated:YES];
                                     });
                                
                                 }
                             }] resume];
                }
            }] resume];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)search:(id)sender {
    [self.view endEditing:YES];
    
    self.city1TextField.text = [[self.city1TextField.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
                                componentsJoinedByString:@""];
    self.city2TextField.text = [[self.city2TextField.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
                                componentsJoinedByString:@""];
    
    [self httpGetRequestWhitCity1:[NSString stringWithFormat:@"%@", self.city1TextField.text] city2:[NSString stringWithFormat:@"%@", self.city2TextField.text]];
}


@end
