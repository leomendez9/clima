//
//  SearchViewController.h
//  Clima
//
//  Created by Leonardo Mendez on 10/02/16.
//  Copyright © 2016 Leonardo Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *city1TextField;
@property (weak, nonatomic) IBOutlet UITextField *city2TextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *city1Label;
@property (weak, nonatomic) IBOutlet UILabel *city2Label;
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;

@end
